<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('landingpage');

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/post-add',[
    'uses'=> 'AdvertController@index',
    'as'=>'postPage'
])->middleware('auth');

Route::get('/getLgas/{id}','AdvertController@getLgas');

Route::get('/getSubcategory/{id}','AdvertController@getSubcategories');

//This Route Add new Add
Route::post('/post_add',[
    'uses'=>'AdvertController@store',
    'as'=>'postadd'
]);

//this Route gets adverts that is clicked 
Route::get('/display_advert/{id}',[
    'uses'=>'AdvertController@show',
    'as'=>'advertSingle'
]);

// Route::get('image/upload',[
//     'uses'=>'AdvertController@fileCreate',
//     'as'=>'advertImage'
// ]);
// Route::post('image/upload/store','AdvertController@fileStore');
// Route::post('image/delete','AdvertController@fileDestroy');

Route::get('image/upload/{id}','ImageUploadController@fileCreate')->name('createImage');
Route::post('image/upload/store/{id}','ImageUploadController@fileStore')->name('imageUploadStore');
Route::post('image/delete','ImageUploadController@fileDestroy');


//Search Routes 
// Route::get('/autocomplete', 'AutocompleteController@index');
Route::post('/autocomplete/fetch', 'AdvertController@fetch')->name('autocomplete.fetch');