@extends('layout')
@section('content')

<div class="site-header">
    <article class="layout-hero">
        <div class="family-sans  heading-search container d-flex justify-content-center">

        <form>
          <div class="input-group">
            <input type="text" class="search-input form-control col-md-12 col-lg-12" placeholder="Search" name="searchBox" id="searchBox" autocomplete="off">
          <div class="input-group-btn">
            <button class="btn-search btn btn-default" type="submit">
              <i class="gly-search fa fa-search" aria-hidden="true"></i>
            </button>
          </div>
          
        </div>
        <div id="searchItem"></div>
        {{ csrf_field() }}
    </form>
          
                         
<!--                  
    <input type="text" name="country_name" id="country_name" class="form-control input-lg" placeholder="Enter Country Name" style="height:60px;" autocomplete="off"/>
                 
      <div id="countryList"></div> 
-->
    
                       
              
    </div>
    </article>   
</div>
<hr>
<div class="second-nav font-openSans container ">
<ul class="nav" >
  <li class="nav-item" >
    <a class="nav-link" href="#" style="color:black;">Party & Events</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#" style="color:black;">Automobile</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#" style="color:black;">Fashion</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#" style="color:black;">Agriculture</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#" style="color:black;">Electronics</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#" style="color:black;">Furniture</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#" style="color:black;">Services</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#" style="color:black;">Real Estate</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#" style="color:black;">Jobs</a>
  </li>
</ul>
</div>
<hr>
<a href="{{url('/')}}" class="card-anc" style="text-decoration: none;">
<article class="card-img-adv container-fluid" style="width:90%">
    <div class="row" >
      @foreach($adverts as $advert)
      <div class="col-6 col-xs-3 col-sm-3 col-md-3 col-lg-2">  
      <section class="card mb-5">
          <img class="img-adv card-img-top" src = "thumbnail/{{$advert->thumbnail}}" alt="Card image cap">
          <div class="card-body">
            <P class="card-title">{{$advert->title}}</P>
            <p>N{{$advert->price}}/hr</p>
          </div>
          
        </section>
      </div>
      @endforeach
    </div>
</article>
</a>

<br>
<footer class="site-footer">@include('layouts.footer')</footer>
@endsection


