@extends('layout')
@section('content')


<section class="post-add-body">
<p class="page-h-text">POST ADVERT HERE</p>
  <form method="post" action="{{ route('postadd') }}" enctype="multipart/form-data">
  @csrf
    <div class="post-container container">
        
        <section class="form-card card bg-light text-dark">
        
            <div class="card-body">
            
                
                <div class="form-group">
                <label for="category" class="input-label">Category:</label><span class="span-com">*</span>
                    <select class="form-control form-control-sm" name="category" id="category">
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->cat_name}}</option>
                        @endforeach
                    </select>
                    
                </div>

                <div class="form-group">
                <label for="subcategory" class="input-label">Subcategory:</label><span class="span-com">*</span>
                    <select class="form-control form-control-sm" name="subcategory" id="subcategory">
                        <option>Select Subcategory</option>
                    </select>
                </div>

               
                
                
                <div class="form-group">
                <label for="title" class="input-label">Title</label><span class="span-com">*</span>
                    <input type="text" class="form-control form-control-sm" name="title">
                </div>

                <div class="form-group">
                    <label for="description" class="input-label">Description:</label><span class="span-com">*</span>
                    <textarea class="form-control" rows="5"  name="description"></textarea>
                </div>
               

                <div class="form-group">
                <label for="price" class="input-label">Price</label><span class="span-com">*</span>
                    <input type="text" class="form-control form-control-sm" name="price">
                </div> 

                <div class="form-group">
                    <label for="file" class="input-label">Image (.jpp, .png only):</label><span class="span-com">*</span>
                    <input type="file" name="filename" class="form-control">
                </div>           
                </div>
        </section>


            <section class="form-card card bg-light text-dark" style="margin-top:2rem;">
            
                <div class="card-body">
                    <p>Contact details for your Advert</p>
                    <p>Name: {{auth::user()->name}}</p>
                    <p>Phone: {{auth::user()->phone}}</p>
                    
                <div class="form-group">
                <label for="region" class="input-label">Region:</label><span class="span-com">*</span>
                    <select class="form-control form-control-sm" name="region" id="region">
                        @foreach($states as $state)
                        <option value="{{$state->id}}">{{$state->state_name}}</option>
                        @endforeach
                    </select>
                </div>

                    <div class="form-group">
                    <label for="place" class="input-label">Place:</label><span class="span-com">*</span>
                        <select class="form-control form-control-sm" name="place">
                            <option>Select Place</option>
                        </select>
                    </div>
                    </div>
            </section>


            <section class="form-card card bg-light text-dark" style="margin-top:2rem">
            
            <div class="card-body">
                <p>Promote Your Advert</p>
                
                

              
                </div>
            </section>
            <br>

            <div class="form-group text-center">
                <button type="submit"  class="add-btn btn btn-warning">Submit Advert</button>
            </div>
        </div>

     


     

     
                
    
    </form>
</section>



@endsection

