<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'RentAnyThing') }}</title>

        <title>Rent|Anything</title>

        <link rel="icon" type="image/png" href="{{ url('img/home.png') }} ">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">


        <!--Import Google Icon Font Materialize-->
        <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
        <!-- Compiled and minified CSS Materialize-->
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"> -->
        <!-- Compiled and minified JavaScript Materialize -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->

        
        <script defer src="{{asset('js/lib/fontawesome-all.min.js')}}"></script>
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/animate.css')}}">
        <!-- Custom CSS -->
        <link href="{{asset('css/custom.css')}}" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <!-- <script defer src="{{asset('js/jquery.js')}}"></script>   -->


        <!--DropZone -->
        
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/dropzone.js"></script>

    </head>
    <body>
    
    <header id="page-hero" class="site-header"> @include('layouts.header')</header>

    

        @yield('content')

       
        
    <!-- VUE JS App JS  -->
    <script src="/js/app.js"></script>

    <!-- <script src="{{asset('js/lib/jquery.min.js')}}"></script> -->
    <script defer src="{{asset('js/jquery.js')}}"></script> 
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/lib/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/script.js')}}"></script>

    <script>
    $(document).ready(function(){
        $('select[name="region"]').on('change', function(){
            var region_id = $(this).val();
            if(region_id){
                
                $.ajax({
                    url: '/getLgas/'+region_id,
                    type: 'GET',
                    dataType: 'json',
                    success: function(data){
                        console.log(data);

                        $('select[name="place"]').empty();
                        $.each(data, function(key, value){
                            $('select[name="place"]')
                                .append('<option value="'+key+'">'+ value + '</option>');
                        });
                    }
                });
            } else{
                $('select[name="place"]').empty();
            }
        });
    });
</script>


<script>
    $(document).ready(function(){
        $('select[name="category"]').on('change', function(){
            var category_id = $(this).val();
            if(category_id){
                $.ajax({
                    url: '/getSubcategory/'+category_id,
                    type: 'GET',
                    dataType: 'json',
                    success: function(data){
                        console.log(data);

                        $('select[name="subcategory"]').empty();
                        $.each(data, function(key, value){
                            $('select[name="subcategory"]')
                                .append('<option value="'+key+'">'+ value + '</option>');
                        });
                    }
                });
            } else{
                $('select[name="subcategory"]').empty();
            }
        });
    });
</script>



<!-- Search Script -->
<script>
$(document).ready(function(){

 $('#searchBox').keyup(function(){ 
        var query = $(this).val();
        if(query != '')
        {
         var _token = $('input[name="_token"]').val();
         $.ajax({
          url:"{{ route('autocomplete.fetch') }}",
          method:"POST",
          data:{query:query, _token:_token},
          success:function(data){
           $('#searchItem').fadeIn();  
           $('#searchItem').html(data);
          }
         });
        }
    });

    $(document).on('click', 'li', function(){  
        $('#searchBox').val($(this).text());  
        $('#searchItem').fadeOut();  
    });  

});
</script>


    </body>
</html>
