<div class="container">

<div class="row">
	

<section class="footer-section col-12 col-sm-6 col-md-3 col-lg-3">
		<p class="links-heading">FAQ</p>
		<nav class="footer-list">
			<ul class="footer-links">
				<li>PRIVACY</li>
				<li>RENTING TIPS</li>
				<li>CATEGORIES</li>
				
			</ul>
		</nav>
	</section>

	<section class="footer-section col-12 col-sm-6 col-md-3 col-lg-3">
		<p class="links-heading">COMPANY</p>
		<nav>
			<ul class="footer-links">
				<li>ABOUT US</li>
				<li>POLICY</li>
				<li>CATEGORIES</li>
				<li>RENTING TIPS</li>
				
			</ul>
		</nav>
	</section>

	<section class="footer-section col-12 col-sm-6 col-md-3 col-lg-3">
		
		<nav>
			<ul class="footer-links">
				<li>
				<section class="footer-section footer-sec1 col-12 col-sm-12 col-md-12 col-lg-12">
					<a href=""><img src = "{{asset('img/googleplay.png')}}" title = "Convergence studio" style="height:50px; width: 70%;" alt = "Convergence studio"/></a>
				</section>

				</li>
				<li class="footer-row">
				<section class="footer-social col-12 col-sm-6 col-md-4 col-lg-4">
					<a class="social-icon" href=""><i class="fab fa-twitter"></i></a>
					<a class="social-icon" href=""><i class="fab fa-facebook"></i></a>
					<a class="social-icon" href=""><i class="fab fa-linkedin"></i></a>
					<a class="social-icon" href=""><i class="fab fa-instagram"></i></a>
				</section>
				</li>
			</ul>
		</nav>
	</section>


	<section class="footer-section col-12 col-sm-6 col-md-3 col-lg-3">
		<p class="links-heading">Payment method</p>
		<nav>
			<ul class="footer-links">
				<li><a href=""><img src = "{{asset('img/visa.png')}}" title = "Rentanything" style="height:15px; width: 30%;" alt = "Rentanything"/></a></i></li>
				<li><a href=""><img src = "{{asset('img/master.png')}}" title = "Rentanything" style="height:30px; width: 30%;" alt = "Rentanything"/></a></i></li>				
			</ul>
		</nav>
	</section>
</div>
<hr style="background-color: white;">
	
</div>


<section class="footer-yellow container-fluid">
<div class="row">
    <section class="footer-trademark col-12 col-sm-12 col-md-12 col-lg-12">
		<p>copyright &copy; 2019 rentanything.com</p>
	</section>
</div>
</section>