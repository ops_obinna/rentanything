<nav class="site-nav family-sans text-uppercase navbar navbar-expand-md  fixed-top navbar-dark">
      <div class="container-fluid">
        <a class="navbar-brand" href="{{url('/')}}"><img src = "{{asset('img/logo.png')}}" title = "RentAnyThing" style="height:150%" alt = "Convergence studio"/></a>
        
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#myTogglerNav" aria-controls="#myTogglerNav"
        aria-controls="#myTogglerNav" aria-label="Toggle Navigation"><span class="navbar-toggler-icon"></span></button>


        <section class="collapse navbar-collapse" id="myTogglerNav">
        <div class="navbar-nav">
        <a class="nav-item nav-link" href="{{url('/post-add')}}"><button type="submit"  class="search-btn btn btn-primary">POST ADD</button></a>
        <a class="nav-item nav-link"><span class="link-sep">&vert;</span></a>
        <a class="nav-item nav-link" href="#page-multicolumn">Help</a>
        <a class="nav-item nav-link"><span class="link-sep">&vert;</span></a>
         
        @guest
                            
        <a class="nav-item nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
               <a class="nav-item nav-link"><span class="link-sep">&vert;</span></a>             
        @if (Route::has('register'))
          <a class="nav-item nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>                      
        @endif
        @else
          <li class="nav-item dropdown">
          <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" style="color:#ffc107;" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
          {{ Auth::user()->name }} <span class="caret"></span>
          </a>

          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

          <a class="dropdown-item" href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              {{ __('My account') }}
          </a>
          <a class="dropdown-item" href="{{ route('logout') }}"
              onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">
              {{ __('Logout') }}
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
          </form>
          </div>
          </li>
        @endguest



       
      </div>
        </section>
      </div>
  </nav>