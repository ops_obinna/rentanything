
@extends('layouts.landingreglogin')
@section('main')

<div class="family-sans layout-image">

@if(
    isset(request()->session()->get('userWrites')[0]['filled']) && request()->session()->get('userWrites')[0]['filled'] == true)
    <br>
    <br>

    <div class="alert alert-success alert-dismissable" role="alert">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      Thank you for providing others with useful information about this service, please sign in below to submit your review.
    </div>

    <br>
    @endif

    <div class="container-fluid dark-bg">
        

		<article class="image-article row"> 
			 <figure class=" mx-auto  col-xs-2 col-sm-6 col-md-4  col-lg-3 py-3">
            <a href="{{ url('/') }}"> <img src="{{asset('/img/logo.png')}}" class="login-image img-fluid figure-img" alt="Company-Logo" ></a>
       </figure>
    </article>
              
		   	
		   	<article class="row">
               <section class="form-layout  col-10  col-md-6 col-lg-6 col-xl-4">
               <p class="text-center py-4">Login with your Email</p>
                 <form method="post" action="{{ route('login') }}">
                
                 @csrf

				  <div class="form-gn  roup">
				    <label for="email">Email</label>
				   <input type="text" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                   @error('email')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                    @enderror
				  </div>
				 
				   
 
				  <div class="form-group">
				    <label for="phone">Password</label>
				   <input id="password" type="password"  class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                   @error('password')
                     <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                     </span>
                    @enderror
				  </div>


                  <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                         </label>
                    </div>
				  </div>


				   <div class="form-group">
				   <input type="submit" class="form-control btn bg-warning" value="Login">
				  </div>
				  
				  
                </form>


                <div class="row">             
               <section class=" col-10  col-md-6 col-lg-12 col-xl-12">
                    <div class="join-us text-center">
                        <p><a class="text-success" href="{{ route('password.request') }}">Forget Password? Reset here</a></p>
                    </div>
              </section>
              </div>

                <span style="color:yellow">&#8213;</span>

                <article class="social-area row">
                <section class="">
                                       
                    <p> Login using social account</p>


                    <div class="social-login container">
                    <article class="row">
                            <div class="social-btn col-xs-12 ">
                                <a href = "{{url('/login/facebook')}}" class="btn-facebook login-icons"> <i class="fab fa-facebook"></i> Facebook</a>
                            </div>
                            <div class="social-btn col-xs-12" >
                                <a href = "{{url('/login/google')}}" class="btn-google login-icons"> <i class="fab fa-google"></i> Google</a>           
                            
                        </article>
                    </div>
                    
                
                </section>
            </article>
                

              </section>
            </article>

               <div class="row">             
               <section class=" col-10  col-md-6 col-lg-6 col-xl-4">
                    <div class="form-layout join-us">
                        <p><a href="{{ url('/register') }}">Create Account</a></p>
                    </div>
              </section>
              </div>

             
    </div>
</div>

@endsection


