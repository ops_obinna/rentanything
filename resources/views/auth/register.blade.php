@extends('layouts.landingreglogin')
@section('main')

<div class="family-sans layout-image">


    <div class="container-fluid dark-bg">
        

		<article class="image-article row"> 
		   <figure class=" mx-auto  col-xs-2 col-sm-6 col-md-4  col-lg-3 py-3">
            <a href="{{ url('/') }}"> <img src="{{asset('/img/logo.png')}}" class="login-image img-fluid figure-img" alt="Company-Logo" ></a>
         </figure>
        </article>
              
		   	
		<article class="row">
               <section class="form-layout  col-10  col-md-6 col-lg-6 col-xl-4">
               <p class="text-center py-4">Sign up to convergence</p>

                <article class="social-area row">
                    <section class="">
                                        
                       <div class="social-login container">
                        <article class="row">
                                <div class="social-btn col-xs-12 ">
                                    <a href = "{{url('/login/facebook')}}" class="btn-facebook login-icons"> <i class="fab fa-facebook"></i> Facebook</a>
                                </div>
                                <div class="social-btn col-xs-12" >
                                    <a href = "{{url('/login/google')}}" class="btn-google login-icons"> <i class="fab fa-google"></i> Google</a>           
                                
                            </article>
                        </div>
                    </section>
                    
                </article>
                <p class="text-center"> Creat account using your email</p>
                <form method="POST" action="{{ route('register') }}">
                
                 @csrf

                 <div class="row">
                 
                 <div class="form-gn  roup col-12 col-md-6">
				    <label for="name">Full name</label>
				   <input type="text" id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                   @error('name')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                    @enderror
				  </div>

                  <div class="form-gn  roup col-12 col-md-6">
				    <label for="username">Preffered username</label>
				   <input type="text" id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

                   @error('username')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                    @enderror
				  </div>

                 </div>


                  <div class="form-gn  roup">
				    <label for="email">Phone</label>
				   <input type="text" id="phone" type="email" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>

                   @error('phone')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                    @enderror
				  </div>

				  <div class="form-gn  roup">
				    <label for="email">Email</label>
				   <input type="text" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                   @error('email')
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                    </span>
                    @enderror
				  </div>
				 
				   
 
				  <div class="form-group">
				    <label for="phone">Password</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
				  </div>

                  <div class="form-group">
				    <label for="password-confirm">Comfirm password</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
				  </div>


                  <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="terms" id="terms" required>
                        <label class="form-check-label" for="terms">
                            {{ __('I accept the') }} <a href="">terms and use</a>
                         </label>
                    </div>
				 </div>

				   <div class="form-group">
				   <input type="submit" class="form-control btn bg-warning text-light" value="Sign up!">
				  </div>
				  
				  
                </form>

              </section>
        </article>


        <div class="row">             
               <section class=" col-10  col-md-6 col-lg-6 col-xl-4">
                    <div class="join-us text-center">
                        <p class="text-white my-4">Already have an account?<a class="text-warning" href="{{ route('login') }}"> Log in!</a></p>
                    </div>
              </section>
              </div>

    </div>
</div>

@endsection