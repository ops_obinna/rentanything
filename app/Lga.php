<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Advert;
use App\State;

class Lga extends Model
{
   
     //
     public function state(){
        return $this->belongsTo('App\State');
    }
    public function advert(){
        return $this->hasMany('App\Advert');
    }
}
