<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ImageUpload;
use App\Advert;
use Auth;

class ImageUploadController extends Controller
{
    //

    public function fileCreate($id)
    {   //$companies = Company::where('user_id',$user_id)->first();
        //$service_id = $companies->service_id;

        $advert = Advert::where('advertUUID', $id)->first();
        
        return view('pages.imageupload')->with('advert',$advert);
    }

    public function fileStore(Request $request, $id)
    {
        $advert = Advert::find($id);
        
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('images'),$imageName);
        
        $imageUpload = new ImageUpload();
        $imageUpload->filename = $imageName;
        $imageUpload->user_id = $advert->user_id;
        $imageUpload->advertUUID_id = $advert->advertUUID;
        $imageUpload->advert_id = $advert->id;
        $imageUpload->save();
        return response()->json(['success'=>$imageName]);
    }
}
