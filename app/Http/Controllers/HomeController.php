<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Advert;
use App\ImageUpload;
use App\User;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $adverts = Advert::Advert();
        
        $viewData = [
            'adverts'=>$adverts,
        ];
    
        return view('pages.landing', $viewData);
    }

}
