<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;
use App\Lga;
use App\Category;
use App\Subcategory;
use App\Advert;
use App\AdvertImage;
use Image;
use Auth;
use DB;


class AdvertController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except('fetch','show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $state = State::AllState();
        $category = Category::all();
        
        $viewData = [
            'states'=>$state,
            'categories'=>$category,
    	];
        return view('pages.postadd', $viewData);
    }

    public function getLgas($id){
        $lgas = Lga::where('states_id', $id)->pluck('place', 'id');
        return json_encode($lgas);
    }


    public function getSubcategories($id){
        $subcategories = Subcategory::where('categories_id', $id)->pluck('sub_cat_name', 'id');
        return json_encode($subcategories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'description' => 'required',
            'category'=>'required',
            'subcategory'=>'required',
            'price'=>'required',
            'region'=>'required',
            'place'=>'required',
            'filename' => 'image|required|mimes:jpeg,png,jpg,gif,svg'
          ]);
            


            $originalImage= $request->file('filename');
            $thumbnailImage = Image::make($originalImage);
            $thumbnailPath = public_path().'/thumbnail/';
            $originalPath = public_path().'/images/';
            $thumbnailImage->save($originalPath.time().$originalImage->getClientOriginalName());
            $thumbnailImage->resize(300,250);
            $thumbnailImage->save($thumbnailPath.time().$originalImage->getClientOriginalName()); 


            $adverts = new Advert();
            $adverts->title = $request->title;
            $adverts->description = $request->description;
            $adverts->category_id = $request->category;
            $adverts->subcategory_id = $request->subcategory;
            $adverts->state_id = $request->region;
            $adverts->lga_id = $request->place;
            $adverts->price = $request->price;
            $adverts->advertUUID = time().'-'.str_replace(' ', '-', strtolower($request->title));
            $adverts->user_id = Auth::user()->id;
            $adverts->thumbnail = time().$originalImage->getClientOriginalName();
            $adverts->save();


        //   $adverts = Advert::create([
        //     'title'=>$request->title,
        //     'description'=>$request->description,
        //     'category_id'=>$request->category,
        //     'subcategory_id'=>$request->subcategory,
        //     'state_id'=>$request->region,
        //     'lga_id'=>$request->place,
        //     'price'=>$request->price,
        //     'advertUUID'=> time().'-'.str_replace(' ', '-', strtolower($request->title)),
        //     'user_id'=>Auth::user()->id,          
            
        //   ]);

          
          
          return redirect('image/upload/'.$adverts->advertUUID);
         
    }

    function fetch(Request $request)
    {
        if($request->get('query'))
        {
        $query = $request->get('query');
        $data = DB::table('adverts')
            ->where('title', 'LIKE', "%{$query}%")
            ->get();
        $output = '<ul class="dropdown-menu" style="">';
        foreach($data as $row)
        {
        $output .= '
        <a style="color:#000000;text-decoration:none;" href="'.route('advertSingle', $row->advertUUID).'"><li class="alink" style="padding-left:20px;padding-top:10px;width:45em;">'.$row->title.'  <span class="search-d">'.$row->description.'</span>  <i class="fas fa-angle-double-left" style="color:#c3c4c3e6;"></i></li></a>
        ';
        }
        $output .= '</ul>';
        echo $output;
        }
    }
  
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        return view('pages.singleadd');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
