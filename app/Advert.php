<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Category;
use App\Subcategory;

class Advert extends Model
{
    //
    protected $fillable = ['title','description','category_id','subcategory_id','state_id','lga_id','image_uploaded','price','advertUUID','user_id'];

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function subcategory(){
        return $this->belongsTo('App\Subcategory');
    }

    public function state(){
        return $this->belongsTo('App\State');
    }
    public function lga(){
        return $this->belongsTo('App\Lga');
    }
    public function user(){
        return $this->belongsTo('App\User');
    }

    public function imageUploads(){
        return $this->hasMany('App\ImageUpload');   
    }


    //fetches Adverts
    public function scopeAdvert($query){
    	return $query = Advert::all();
    }

}
