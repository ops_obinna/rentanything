<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Advert;

class Subcategory extends Model
{
    //
    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function advert(){
        return $this->hasMany('App\Advert');
    }
}
