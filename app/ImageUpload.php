<?php

namespace App;
use App\User;
use App\Advert;
use DB;

use Illuminate\Database\Eloquent\Model;

class ImageUpload extends Model
{
    //
    protected $fillable = ['advert_id','user_id','filename','advertUUID_id'];

    
    public function advert(){
        return $this->belongsTo('App\Advert');
    }


    public function scopeAllImages($query){
    	return $query = ImageUpload::all();
    }

}
