<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Advert;

class Category extends Model
{
    //

    public function subcategory()
    {
        return $this->hasMany('App\Subcategory');
    }

    //Category Has many Product
    public function advert(){
        return $this->hasMany('App\Advert');
    }

    
}
